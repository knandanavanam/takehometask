How to run the application:

1. You should have uncompressed the project by now.
2. Open terminal
3. Navigate to project home directory
4. run command > export JAVA_OPTS="-Xmx4G -Xms4G"
5. run command > sbt
6. run command > run <arg1> <arg2> ..... <argN>

Please run TakeHome class with 6 arguments in the following order:

1. input followersFile
2. input jamsFile
3. input likesFile
4. output file path popularJams
5. output file path mostLikedArtists
6. output file path similarUserPairs

Output Results:
1. Top five popular jams => artist, title, JamCount
2. Top five liked artists => artist, likesCount
3. Top five similar user pairs => userA, userB, likesCommonCount, followedCommonCount, totalCount

Example:
run /Users/knandanavanam/Downloads/thisismyjam-datadump/archive/followers.tsv /Users/knandanavanam/Downloads/thisismyjam-datadump/archive/jams.tsv /Users/knandanavanam/Downloads/thisismyjam-datadump/archive/likes.tsv /tmp/popularJams.tsv /tmp/mostLikedArtists.tsv /tmp/similarUserPairs.tsv

1. What were the top 5 most popular jams of all time? Where the popularity is defined by how often a track is used as a
jam. Let’s assume a track is identified by artist and title. For each result please provide artist and title.

Answer: The data exists in jams.tsv file where each record is a jam record. So, I grouped the requests by artist and
title with jamCount aggregate value order by jamCount descending and then limit to 5. The query running fairly quickly.

2. What were the top 5 most liked artists. An artist is liked if a jam by that artists was liked by a user.

Answer: The data exists in likes.tsv file where each record is a like by user to jam. I joined likes with jams data.
So, we can link jam ids with Artist. I then grouped by artist with likesCount aggregation order by likesCount descending.

3. What were the top 5 most similar user pairs of This is My Jam. Please suggest and explain your choice of similarity
measure. Please provide user_ids.

Answer: Similarity between users can be defined by number of common jam likes or followed artists with weights.
This is a cross-product/collaborative filtering problem. O(n^3) complexity. There are some ML techniques I can apply like
PCA, LSH (Takes time for analysis and hyper parameter tuning). So, I wanted to go with my own stats and intuition.

Issues faced:
1. Without filtering anything, I got memory exceptions
2. When I solved the problem with lower bound removal (including > average aggregation). I got only the outliers of the upper bound.

So I solved it as follows
1. First I got the stats of the followed, followers, likes, jams that are liked (Stats.scala)
2. Used Average and Standard deviation to filter out the outliers
3. I found the similarity between two users as number of common jam likes + common followed artists (equal weight and equi-join)

I haven't added any weights to the aggregations. I think "followed" aggregation can be added weight as it is more valuable.
Also, I have joined both aggregations as equi-join. We can also perform full-outer join.
