package com.spotify

import java.io.{BufferedWriter, File, FileWriter}

import org.apache.spark.SparkConf
import org.apache.spark.sql.types.{IntegerType, StringType, StructField, StructType}
import org.apache.spark.sql.{Row, SparkSession}

object TakeHome {

  def main(args: Array[String]): Unit = {
    val (followersFile, jamsFile, likesFile) = inputLocations(args)
    val (popularJams, mostLikedArtists, similarUserPairs) = outputLocations(args)

    val conf = new SparkConf()
      .setMaster("local[*]")

    val spark = SparkSession.builder()
      .appName("TakeHomeSpark")
      .config(conf)
      .getOrCreate()

    import spark.implicits._

    spark.read.format("csv").option("delimiter", "\t").option("header", "true").load(followersFile).createOrReplaceTempView("followers")
    spark.read.format("csv").option("delimiter", "\t").option("header", "true").load(jamsFile).createOrReplaceTempView("jams")
    spark.read.format("csv").option("delimiter", "\t").option("header", "true").load(likesFile).createOrReplaceTempView("likes")

    def topFivePopularJams = {
      val topFivePopularJams = spark.sql(
        """select artist, title, count(*) as jamCount
          |from jams
          |group by artist, title
          |order by jamCount desc
          |limit 5""".stripMargin).collect()
      println("popularJams:\n" + topFivePopularJams.map(r => r.mkString(",")).mkString("\n"))
      saveFile(topFivePopularJams, popularJams)
    }

    def topFiveMostLikedArtists = {
      val topFiveLikedArtists = spark.sql(
        """select j.artist, count(*) as likesCount
          |from jams j join likes l on j.jam_id == l.jam_id
          |group by j.artist
          |order by likesCount desc
          |limit 5""".stripMargin).collect()
      println("mostLikedArtists:\n" + topFiveLikedArtists.map(r => r.mkString(",")).mkString("\n"))
      saveFile(topFiveLikedArtists, mostLikedArtists)
    }

    def mostSimilarUserPairs = {
      val limitFollowed = 17 + 242
      val artistsValid = spark.sparkContext.broadcast(spark.sql(s"select followed_user_id, count(*) as followersCount from followers group by followed_user_id having followersCount <= ${limitFollowed}").map(r => r.getAs[String](0)).collect().toSet)
      val limitFollowers = 17 + 92
      val usersValid = spark.sparkContext.broadcast(spark.sql(s"select follower_user_id, count(*) as followedCount from followers group by follower_user_id having followedCount <= ${limitFollowers}").map(r => r.getAs[String](0)).collect().toSet)

      val userWithFollowers = spark.sql("select followed_user_id, follower_user_id from followers").filter(r => artistsValid.value.contains(r.getAs[String]("followed_user_id")) && usersValid.value.contains(r.getAs[String]("follower_user_id"))).rdd
        .groupBy(r => r.getAs[String]("followed_user_id"))
        .mapValues(r => r.map(rec => rec.getAs[String](1)).toSet.toList.sorted)

      val fSchema = new StructType()
        .add(StructField("userAF", StringType, false))
        .add(StructField("userBF", StringType, false))
        .add(StructField("fScore", IntegerType, false))
      spark.createDataFrame(userWithFollowers.flatMap(r => (r._2 cross r._2).filter(r => r._1.compareTo(r._2) < 0)
        .map(r => ((r._1, r._2), 1)))
        .reduceByKey((a, b) => a + b)
        .map(r => Row(r._1._1, r._1._2, r._2)), fSchema)
        .createOrReplaceTempView("userFollowPairs")

      val limitUserLikes = 102 + 1195
      val validUsers = spark.sql(s"select user_id, count(*) as likesCount from likes group by user_id having likesCount <= ${limitUserLikes}").map(r => r.getAs[String](0)).collect().toSet
      val limitJamLikes = 5 + 9
      val validJams = spark.sql(s"select jam_id, count(*) as jamLikes from likes group by jam_id having jamLikes <= ${limitJamLikes}").map(r => r.getAs[String](0)).collect().toSet

      val userWithLikes = spark.sql(s"select jam_id, user_id from likes").filter(r => validUsers.contains(r.getAs[String]("user_id")) && validJams.contains(r.getAs[String]("jam_id"))).rdd
        .groupBy(r => r.getAs[String]("jam_id"))
        .mapValues(r => r.map(rec => rec.getAs[String](1)).toSet.toList.sorted)

      val lSchema = new StructType()
        .add(StructField("userAL", StringType, false))
        .add(StructField("userBL", StringType, false))
        .add(StructField("lScore", IntegerType, false))
      spark.createDataFrame(userWithLikes.flatMap(r => (r._2 cross r._2).filter(r => r._1.compareTo(r._2) < 0)
        .map(r => ((r._1, r._2), 1)))
        .reduceByKey((a, b) => a + b)
        .map(r => Row(r._1._1, r._1._2, r._2)), lSchema)
        .createOrReplaceTempView("userLikePairs")

      val userPairs = spark.sql(s"select userAL, userBL, lScore, fScore, (lScore + fScore) as score from userLikePairs join userFollowPairs on (userAL == userAF and userBL == userBF) order by score desc")
        .take(5)
      println("similarUserPairs:\n" + userPairs.map(r => r.mkString(",")).mkString("\n"))
      saveFile(userPairs, similarUserPairs)
    }

    topFivePopularJams
    topFiveMostLikedArtists
    mostSimilarUserPairs

    spark.close()
  }

  def outputLocations(args: Array[String]) = {
    (args(3), args(4), args(5))
  }

  def inputLocations(args: Array[String]) = {
    (args(0), args(1), args(2))
  }

  def saveFile(rows: Array[Row], filePath: String) = {
    val res = rows.map(r => r.mkString(","))
    val writer = new BufferedWriter(new FileWriter(new File(filePath)))
    res.foreach(r => {
      writer.write(r)
      writer.newLine()
    })
    writer.close()
  }

  implicit class Crossable[String](xs: List[String]) {
    def cross[String](ys: List[String]) = for { x <- xs; y <- ys} yield (x, y)
  }
}
