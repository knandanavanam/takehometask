package com.spotify

import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

object Stats {
  def main(args: Array[String]): Unit = {
    val (followersFile, jamsFile, likesFile) = inputLocations(args)
    val (popularJams, mostLikedArtists, similarUserPairs) = outputLocations(args)

    val conf = new SparkConf()
      .setMaster("local[*]")

    val spark = SparkSession.builder()
      .appName("TakeHomeSpark")
      .config(conf)
      .getOrCreate()

    import spark.implicits._

    spark.read.format("csv").option("delimiter", "\t").option("header", "true").load(followersFile).createOrReplaceTempView("followers")
    spark.read.format("csv").option("delimiter", "\t").option("header", "true").load(jamsFile).createOrReplaceTempView("jams")
    spark.read.format("csv").option("delimiter", "\t").option("header", "true").load(likesFile).createOrReplaceTempView("likes")

    def statsOnFollowers() = {
      val totalPairs = spark.sql("select * from followers").count()
      val followed = spark.sql(s"select distinct(followed_user_id) from followers").count()
      val avgFollowers = spark.sql(s"select followed_user_id, count(*) as followersCount from followers group by followed_user_id").map(r => r.getAs[Long](1)).reduce((a, b) => a + b) / followed
      val stdFollowed = spark.sql(s"select followed_user_id, count(*) as followersCount from followers group by followed_user_id").map(r => r.getAs[Long](1))

      val followers = spark.sql(s"select distinct(follower_user_id) from followers").count()
      val avgFollowed = spark.sql(s"select follower_user_id, count(*) as followedCount from followers group by follower_user_id").map(r => r.getAs[Long](1)).reduce((a, b) => a + b) / followers
      val stdFollower = spark.sql(s"select follower_user_id, count(*) as followedCount from followers group by follower_user_id").map(r => r.getAs[Long](1))

      val limitFollowed = 17 + 242
      val artistsValid = spark.sparkContext.broadcast(spark.sql(s"select followed_user_id, count(*) as followersCount from followers group by followed_user_id having followersCount <= ${limitFollowed}").map(r => r.getAs[String](0)).collect().toSet)
      val limitFollowers = 17 + 92
      val usersValid = spark.sparkContext.broadcast(spark.sql(s"select follower_user_id, count(*) as followedCount from followers group by follower_user_id having followedCount <= ${limitFollowers}").map(r => r.getAs[String](0)).collect().toSet)

      val pairsValid = spark.sql("select followed_user_id, follower_user_id from followers").filter(r => artistsValid.value.contains(r.getAs[String](0)) && usersValid.value.contains(r.getAs[String](1)))

      val validArtists = artistsValid.value.size
      val validUsers = usersValid.value.size
      val validPairs = pairsValid.count()

      stdFollowed.describe().show()
      stdFollower.describe().show()
      println(s"totalRecords: ${totalPairs}")
      println(s"distinctFollowers: ${followers}")
      println(s"distinctFollowed: ${followed}")
      println(s"avgFollowers: ${avgFollowers}")
      println(s"avgFollowed: ${avgFollowed}")
      println(s"validFollowed: ${validArtists}")
      println(s"validFollowers: ${validUsers}")

      println(s"validPairs: ${validPairs}")
    }

    def statsOnLikes() = {
      val totalLikes = spark.sql("select * from likes").count()
      val distinctUsers = spark.sql("select distinct(user_id) from likes").count()
      val distinctJams = spark.sql("select distinct(jam_id) from likes").count()

      val avgLikes = spark.sql("select user_id, count(*) as likesCount from likes group by user_id").map(r => r.getAs[Long](1)).reduce((a, b) => a + b) / distinctUsers
      val statLikes = spark.sql("select user_id, count(*) as likesCount from likes group by user_id").map(r => r.getAs[Long](1))
      val avgLiked = spark.sql("select jam_id, count(*) as jamLikes from likes group by jam_id").map(r => r.getAs[Long](1)).reduce((a, b) => a + b) / distinctJams
      val statLiked = spark.sql("select jam_id, count(*) as jamLikes from likes group by jam_id").map(r => r.getAs[Long](1))

      val limitUserLikes = 102 + 1195
      val validUsers = spark.sql(s"select user_id, count(*) as likesCount from likes group by user_id having likesCount <= ${limitUserLikes}").map(r => r.getAs[String](0)).collect().toSet
      val limitJamLikes = 5 + 9
      val validJams = spark.sql(s"select jam_id, count(*) as jamLikes from likes group by jam_id having jamLikes <= ${limitJamLikes}").map(r => r.getAs[String](0)).collect().toSet

      val validLikes = spark.sql(s"select user_id, jam_id from likes").filter(r => validUsers.contains(r.getAs[String](0)) && validJams.contains(r.getAs[String](1))).count()

      statLikes.describe().show()
      statLiked.describe().show()
      println(s"totalLikes: ${totalLikes}")
      println(s"distinctUsers: ${distinctUsers}")
      println(s"distinctJams: ${distinctJams}")
      println(s"avgLikes: ${avgLikes}")
      println(s"avgLiked: ${avgLiked}")
      println(s"validUsers: ${validUsers.size}")
      println(s"validJams: ${validJams.size}")
      println(s"validLikes: ${validLikes}")
    }

    statsOnFollowers()
    statsOnLikes()
  }

  def outputLocations(args: Array[String]) = {
    (args(3), args(4), args(5))
  }

  def inputLocations(args: Array[String]) = {
    (args(0), args(1), args(2))
  }
}
