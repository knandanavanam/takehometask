package com.spotify

import org.scalatest.FunSuite

class TakeHomeTest extends FunSuite {

  test("testAll") {
    val followersFile = classOf[TakeHomeTest].getResource("/followers.tsv").toURI.toURL.getPath
    val jamsFile = classOf[TakeHomeTest].getResource("/jams.tsv").toURI.toURL.getPath
    val likesFile = classOf[TakeHomeTest].getResource("/likes.tsv").toURI.toURL.getPath

    val popularJams = "/tmp/popularJams.tsv"
    val mostLikedArtists = "/tmp/mostLikedArtists.tsv"
    val similarUserPairs = "/tmp/similarUserPairs.tsv"

    val args = Array(followersFile, jamsFile, likesFile, popularJams, mostLikedArtists, similarUserPairs)
    TakeHome.main(args)
  }

  test("testRealFiles") {
    val followersFile = "/Users/knandanavanam/Downloads/thisismyjam-datadump/archive/followers.tsv"
    val jamsFile = "/Users/knandanavanam/Downloads/thisismyjam-datadump/archive/jams.tsv"
    val likesFile = "/Users/knandanavanam/Downloads/thisismyjam-datadump/archive/likes.tsv"

    val popularJams = "/tmp/popularJams.tsv"
    val mostLikedArtists = "/tmp/mostLikedArtists.tsv"
    val similarUserPairs = "/tmp/similarUserPairs.tsv"

    val args = Array(followersFile, jamsFile, likesFile, popularJams, mostLikedArtists, similarUserPairs)
    TakeHome.main(args)
  }

  test("Stats") {
    val followersFile = "/Users/knandanavanam/Downloads/thisismyjam-datadump/archive/followers.tsv"
    val jamsFile = "/Users/knandanavanam/Downloads/thisismyjam-datadump/archive/jams.tsv"
    val likesFile = "/Users/knandanavanam/Downloads/thisismyjam-datadump/archive/likes.tsv"

    val popularJams = "/tmp/sample/popularJams"
    val mostLikedArtists = "/tmp/sample/mostLikedArtists"
    val similarUserPairs = "/tmp/sample/similarUserPairs"

    val args = Array(followersFile, jamsFile, likesFile, popularJams, mostLikedArtists, similarUserPairs)
    Stats.main(args)
  }
}
